package pl.softwarewizard;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;


@Path("/example")
@RequestScoped
public class ExampleRestFul {

   @EJB
   ExampleService exampleService;

   @GET
   public String test() {
      return "ejb call: " + exampleService.whoAmI();
   }
}
